=== SlideDeck 3 Personal for WordPress ===
Contributors: SlideDeck
Tags: Slider, dynamic, responsive, image gallery, hbwsl, iPad, jquery, media, photo, pictures, plugin, posts, Search Engine Optimized, seo, skinnable, slide, slide show, slider, slideshow, theme, touch support, video, widget, Flickr, Instagram, 500px, RSS, Pinterest, Google+, YouTube, Vimeo, Dailymotion, Picasa, Dribbble
Requires at least: 3.3
Tested up to: 4.7
Stable tag: trunk
License: GPLv3

Create responsive content sliders on your WordPress blogging platform. Manage SlideDeck content and insert them into templates and posts.

== Description ==

= Responsive Content Slider by SlideDeck 3 =
= Easily create responsive content sliders for your WordPress site without code. Use images & text, plus YouTube, Flickr, Pinterest & more =
SlideDeck 3 for WordPress is a responsive slider plugin that lets you easily create content sliders out of almost any content. Connect to a variety of Content Sources like YouTube, Flickr, WordPress posts and Pinterest to create gorgeous, dynamic sliders in a few clicks - no coding is required.

**Requirements:** WordPress 3.3+, PHP5 and higher

**Important Links:**

* [More Details](https://www.slidedeck.com/)
* [Knowledge Base](https://www.slidedeck.com/docs/)
* [Support](http://support.slidedeck.com/)

== Installation ==

1. Upload the `slidedeck3-personal` folder and all its contents to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Create a new SlideDeck from the “SlideDeck » Manage” menu in the control panel sidebar
4. Insert a SlideDeck in your post or page by clicking on the "Embed a SlideDeck" button above the rich text editor in the post/page view.

You can also place a SlideDeck in your template or theme via the PHP command “do_shortcode()”. Just pass the ID of the SlideDeck you want to render. For example:
`<?php echo do_shortcode( "[SlideDeck2 id=1644]" ); ?>`

Where 1644 is the SlideDeck's ID. You can also see this code snippet when you save a new SlideDeck for the first time.


== Changelog ==
= 4.3 =
Bug Fix: Proto Lens responsive bug fixed for smaller devices.
Bug Fix: Half-moon Lens responsive bug fixed for smaller devices.
Bug Fix: Block Title Lens Navigation arrows updated and responsive bug fixed for the title and description text.
Bug Fix: Reporter Lens Front View bug fixed.
Bug Fix: Layerpro Lens Responsive bug fixed.
Bug Fix: Prime Lens responsive bug fixed for slide title and description.
Bug Fix: Fashion lens responsive bug fixed for the title and description text on smaller devices.
Bug Fix: O-Town Lens responsive bug fixed for smaller devices.
Bug Fix: ToolKit Lens fullwidth responsive bug fixed for smaller devices.
Bug Fix: Removed Notices related to slidedeck overlays.
Bug Fix: Removed Notice related SlideDeck Widget.

= 4.2 =
 Feature Enhancement: New lens added - 'Tiled'. Now you can create a slider with collapsible tiled effect

= 4.1 =
 Feature Enhancement: added functionality for protecting image copying.
 Bug Fix: Layerpro Lens Responsive bug fixed.
 Bug Fix: Layerpro Lens text layer bug fixed.
 Bug Fix: Layerpro Navigation bug fixed.

= 4.0.5 =
 Feature Enhancement: LayerPro Lens - Added Video Background.
 Bug Fix: Dailymotion video integration bug fixed.
 Bug Fix: Reporter Lens navigation bug fixed.
 Bug Fix: O-Town Lens responsive bug fixed.

= 4.0.4 =
 Feature Enhancement: Added Transition Out Effects for LayerPro Lens.
 Bug Fix: Minor CSS errors fixed.

= 4.0.3 =
 Bug Fix: Navigation and front cover bug fix for Tool kit lens.

= 4.0.2 =
 Added Compatibility for LayerPro

= 4.0.1 =
 Bug Fix: Navigation bug fix for mobile view of Tool kit lens.

= 4.0 =
 Feature Enhancement: Added 6 different animation effects for lens - 'Titles'.
 Feature Enhancement: Added 'center' position option for slide-title.
 
= 3.9 =
 Feature Enhancement:Added 7 New Predesigned Templates.

= 3.8 =
 Feature Enhancement:Added Predesigned templates for creating slider.

= 3.7 =
 Feature Enhancement:Change UI Design of o-town lens
 Feature Enhancement:In the o-town lens admin can set the Navigation postion(left/right) 

= 3.6.1 =
  Added link for create instagram client id.

= 3.6 =
  Bug Fix: Instagram token was not getting generated.

= 3.5 =
  Updated version number to keep parity with core. 

= 3.4.9 =
  Bug Fix: Issues for Addon dashboard has fixed.

= 3.4.8 =
  Feature Enhancement: Price tag from WooCommerce dynamic source has been improved.
  Added hooks to add external sources from SlideDeck Addon.
  Bug Fix: If 'Image Scaling' option is set to 'Scale and do not crop' and author avatar gets activated and then it was overlapping slide.

= 3.4.7 =
  Added Lenses support for new source Zenfolio.
  Bug Fix: In tool Kit lens fixed issue related to custom size option not getting modified from its default value.
  Bug Fix: In tool Kit lens fixed issue related to fullwidth size option not not showing suboption as height at first time.

= 3.4.6 =
  Added Lenses support for new source tumblr. 

= 3.4.5 =
 New Feature: Fullwidth option available with toolkit lens.

= 3.4.4 =
 Updated version number to keep parity with other plugin addons.

= 3.4.3 =
  New Feature: Added new option as 'Player Controls' for Tool-Kit Lens, end user can now play and pause slider from frontend.

= 3.4.2 =
  Bug Fix: In Prime lens, Image scaling option 'scale proportionally and do not crop' not working corectly for the images having height less than container.  
  Bug Fix: In case of slide transition as 'slide', last slide is getting displayed incorrectly within first loop fixed for all lenses.  

= 3.4.1 =
  Bug Fix: Slide contents in classic lens with dynamic sources are hiding behind spines when spines show alwayse option selected has been fixed.
  Bug Fix: In case of option 'Image Scaling' as 'scale Proportionally and crop image', first slide is not getting scaled to container has been fixed.

= 3.4.0 =
  New Feature : Added hooks to support Parallax Lens.

= 3.3.6 =
  Feature Enhancement: Added more option and keep configurable - Facebook, Twitter, Google+, LinkedIn, Pininterest

= 3.3.5 =
  Bug Fix: In reporter lens if title is shown completely then elipses will not be appended.
  Bug Fix: Issue related to if start slide is set to last slide then last slide will be shown properly even after click event has fixed.
  Bug Fix: Issue related to Image scaling option set to 'Scale propertionally and do not crop' in case of Custom Video as thumbnail is not getting scaled proportionally has been fixed.
  Bug Fix: In case of custom image, first slide is set to Image scaling option has set to 'Scale Proportionally and crop' then first slide is not getting scaled proportionally has been fixed.
  Bug Fix: If Advanced Option as 'Always load SlideDeck JavaScript/CSS on every page?' is set to OFF and SlideDeck is loaded on page only from Widget then SlideDeck is not working properly and js errors occurs has been fixed. 

= 3.3.4 =
  Enhancement: Enhanced 'Click to Acton (CTA)' feature. Now in addition to custom button, it will support with custom image.
	

= 3.3.3 =
  Bug Fix: Issue related to slides are hiding behind spines in classic lens.
  Bug Fix: Issue related to if start slide is set to last slide then spines not showing properly.
  Bug Fix: Issue related to Image scaling option set to 'Scale propertionally and do not crop' images more than container height are not scaled propertionally.
  Bug Fix: The Js error from admin page is removed regarding SlideDeck.

= 3.3.2 =
  Added enhancement to cta button customization capability.
  Bug Fix: Issue related to flicering spines on mouse hover in classic lens.

= 3.3.1 =
  New Feature: Added new source woocommerce. 
  Now one can show woocommerce products on Slider. 

= 3.3 =
  New Feature : Added Click to Action (CTA) button option for the slidedeck.

= 3.2.6 =
  Bug Fix: Fixed youtube, vimeo and dailymotion video play bug for O-town lens.
  Bug Fix: Issue with image scaling option when set to "Scale Proportionally" is now fixed.
  Bug Fix: Issue related to scaling of first image in slider fixed.
  Bug Fix: In classic Lens, if 'Show Slide Controls' option is set to 'Always' spines are now showing properly.

= 3.2.5 =
  New Feature: Added support for new lens Parfocal.

= 3.2.4 =
 New Feature : Now you can add future posts to SlideDecks using Posts source.
 Bug Fix: fixed issue related Image auto adjust height on mobile devices.

= 3.2.3 =
 New Feature : Now you can Pin It images from SlideDeck to Pininterest.
 New Feature : Now one can manage SlideDecks with an ease using new search by name feature.

 Bug Fix: In O-town lens, Accent color is getting applied to Slide title.

= 3.2.2 =
  Feature Enhancement: Added hooks to enable dynamic plugging of new addons.

= 3.2.1 =
  Feature Enhancement: Prime lens now supports all Dynamic source.
  Bug Fix: For custom source Video slide slidedeck can fetch video meta (e.g.: title, description and thumbnail) from URL.
  Bug Fix: For custom source Image, TextOnly and HTML slidedeck can fetch title and content length.
  Bug Fix: For Dynamic source you can specify number of characters for title and content.

= 3.2.0 =
  New Lens Prime: The new lens Supports
  1. Fullwidth Slider
  2. Slider in LightBox
  3. 23 New Slide Transition effects.
  4. Carousel effect making sure slides do not loop back from last slide to first slide.

  New Feature: SlideDeck now supports European Date Format e.g. : 28th August, 2015
  Bug Fix: In Reorter lens, Titles will now appear in combination of Both cases(lowercase and uppercase).
  Bug Fix: In tool-kit lens, words will not break in case of multiline.
  Bug Fix: Custom css editor will not break after full scrolling.
  Bug Fix: In tool kit lens, navigation Bar will not disappears on mac devices.

= 3.1.1 =
  Added changes to support slidedeck scheduler addon

= 3.1.0 =
  New Feature : Deck level settings added for the slides - user does not need to update each and every image again and again
  New Feature : Scroll and Pinch effect enabled for mobile devices
  New Feature : Enabled ChannelId configuration for Youtube Source.
  New Feature : User can now define own alt text for image  - ALT tag can be updated manually
  Bug Fix : Image scaling option is back
  Bug Fix: Lazy loading issue for Youtube videos fixed
  Bug Fix: Auto Adjust fix for RSS feeds
  Bug Fix : Widget problem for migrated slides from SD2 is fixed
  Bug Fix : HTML slide - text content not seen issue fixed
  Fixed few other minor bugs

= 3.0.0 =
* Initial beta release.
