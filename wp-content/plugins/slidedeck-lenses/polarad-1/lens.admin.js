(function($){
	var ajaxOptions = [
        "options[layout_style]"
	];
	for(i = 0; i < ajaxOptions.length; i++){
		SlideDeckPreview.ajaxOptions.push(ajaxOptions[i]);
	}
	
	$(document).ready(function(){
    	$('#options-layout_style').change(function(event){
    	    if( $(this).val() == 'layout-stacked' ){
    	        $('#options-slideTransition').val('stack');
    	    }else{
    	        $('#options-slideTransition').val('slide');
    	    }
    	});
    	
        SlideDeckPreview.updates['options[show-likes]'] = function($elem, value){
            value = value == 1 ? true : false;
            if(value){
                SlideDeckPreview.elems.slidedeckFrame.addClass(SlideDeckPrefix + 'show-likes');
            } else {
                SlideDeckPreview.elems.slidedeckFrame.removeClass(SlideDeckPrefix + 'show-likes');
            }
        };
        
        SlideDeckPreview.updates['options[show-drop-shadow]'] = function($elem, value){
            value = value == 1 ? true : false;
            if(value){
                SlideDeckPreview.elems.slidedeckFrame.addClass(SlideDeckPrefix + 'show-drop-shadow');
            } else {
                SlideDeckPreview.elems.slidedeckFrame.removeClass(SlideDeckPrefix + 'show-drop-shadow');
            }
        };
        
        SlideDeckPreview.updates['options[show-tape]'] = function($elem, value){
            value = value == 1 ? true : false;
            if(value){
                SlideDeckPreview.elems.slidedeckFrame.addClass(SlideDeckPrefix + 'show-tape');
            } else {
                SlideDeckPreview.elems.slidedeckFrame.removeClass(SlideDeckPrefix + 'show-tape');
            }
        };
        
        SlideDeckPreview.updates['options[arrow-style]'] = function($elem, value){
            $elem.find('option').each(function(){
                if(this.value != value){
                    SlideDeckPreview.elems.slidedeckFrame.removeClass(SlideDeckPrefix + this.value);
                } else {
                    SlideDeckPreview.elems.slidedeckFrame.addClass(SlideDeckPrefix + this.value);
                }
            });
        };

        
	});
})(jQuery);