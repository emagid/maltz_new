<?php

get_header(); ?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>



<div class="upcoming_auctions">
		<div class="featured_in_header">
            <img src="/wp-content/themes/Avada-Child-Theme/images/auctions_icon.png">
			<h1><span><?php the_title(); ?></span></h1>
		</div>

		<div class="auction_holdings">
<?php 
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$data= new WP_Query(array(
      'post_type' => 'sold_auctions',
                'posts_per_page' => 50,
                                                'order' => 'DESC', 
                'meta_key' => '_expiration-date', 
                'orderby' => 'meta_value',
    'paged' => $paged,
));

if($data->have_posts()) :
    while($data->have_posts())  : $data->the_post();?>
            
                        <a id="url_og" href="<?php the_permalink(); ?>">
                <div class="auction_replaced">
                <div class="auction_holder trigger" style="background-image:url(<?php the_field('featured_image'); ?>);">
<!--                    <div class="overlay">-->

                                            <div class="fav_btn_holder">
                    <?php echo do_shortcode('[favorite_button]'); ?>
<!--                        </div>-->
                    </div>

                </div>
                                        <div class="auction_holder_contain" id="modified">
                            <div class="auction_detail">
                                <h4 style="color:#282a2b !important; margin-top:0;"><?php the_title( ); ?></h4>
                                <p style="color:#282a2b !important;"><?php the_field('city/state'); ?></p>
                            </div>
                            <div class="auction_date">
                                <h6><?php the_field('month'); ?><br><span><?php the_field('date'); ?></span></h6>
                            </div>
                            
                        </div>
                </div>
            </a>
            
    <?        
    endwhile;

    $total_pages = $data->max_num_pages;

    if ($total_pages > 1){

        $current_page = max(1, get_query_var('paged'));?>

            <div class="paginate">
       <? echo paginate_links(array(
            'base' => get_pagenum_link(1) . '%_%',
            'format' => '/page/%#%',
            'current' => $current_page,
            'total' => $total_pages,
            'prev_text'    => __('« prev'),
            'next_text'    => __('next »'),
        ));
    }
    ?>   
            </div>
<?php else :?>
<h3><?php _e('404 Error&#58; Not Found', ''); ?></h3>
<?php endif; ?>
<?php wp_reset_postdata();?>
            
            
            
                        <?php
            $args = array(
      'post_type' => 'sold_auctions',
                'posts_per_page' => 100,
                                                'order' => 'DESC', 
                'meta_key' => '_expiration-date', 
                'orderby' => 'meta_value'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>

            
            
                        <?php
            }
                  }
            else  {
            echo 'There are currently no scheduled Previous Auctions auctions. Please visit again soon or join the Maltz Auctions mailing list for future notifications.';
            }
      ?> 

        </div>  
</div> 
    <!-- Trigger to open Modal -->
    


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<link rel="stylesheet" type="text/css" href="/wp-content/themes/Avada/js/libs/iziModal-master/css/iziModal.min.css"/>
    <script type="text/javascript" src="/wp-content/themes/Avada/js/libs/iziModal-master/js/iziModal.min.js"></script>



<!-- Modal structure -->
<!--
<div id="modal" class="subscribe_modal">
<div class="subscribers"  style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/jumbotron_img.png);">
	<h4>Sign up for Maltz Auction Alerts</h4>

	<div class="subscribe_submit">
		<//?php echo do_shortcode('[contact-form-7 id="710302" title="Auction Alerts"]'); ?>
		
	</div>
    
    <span id="url_new"><p>Or continue to auction</p></span>

</div>
-->

</div>

<!--
<script>
$(function() {
    $("a#url_og").click(function() {
        var thisurl = $(this).attr('href');
        
        $("#url_new").html('<a href="' + thisurl + '">' + 'Or continue to auction' + '</a>'); 
        });
    });
</script>

<script>
    
    
$("#modal").iziModal();
    
    $(document).on('click', '.trigger', function (event) {
    event.preventDefault();
    $('#modal').iziModal('open');
        
});
</script>
-->




<div class="subscribers"  style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/blurred_1.jpg);">
	<h4>Maltz Auction Alerts</h4>

	<div class="subscribe_submit">
		<?php echo do_shortcode('[mailmunch-form id="651102"]'); ?>
		
	</div>

</div>

<?php get_footer(); ?>