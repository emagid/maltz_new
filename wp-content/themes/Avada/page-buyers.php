<?php
get_header(); ?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>


<div class="">
    <div class="standard_contain">


		<div class="featured_in_header">
             <h1 ><span><?php the_title(); ?></span></h1>
		</div>

		<div class="about_us_text">
                <?php the_field('content'); ?>
        </div>
        
	</div>
	
</div>

<div class="test_section">

			<div class="featured_in_header">
			<h1><span>Testimonials</span></h1>
		</div>
<div class="wrapper">
<h4>Testimonials from a few of our buyers.</h4>
			
    
            <div class="testimonials_slider" id="buyers">

					<?php
			  $args = array(
			    'post_type' => 'testimonials',
                  'cat' => '25'
			    );
			  $products = new WP_Query( $args );
			  if( $products->have_posts() ) {
			    while( $products->have_posts() ) {
			      $products->the_post();
			?>

				<div class="quote_box">
                    <h6>"</h6>
					<blockquote><?php the_field('quote'); ?></blockquote>
					<p>- <?php the_field('author'); ?><p>
				</div>

				<?php
			    		}
			  		}
				  else {
				    echo 'No Testimonials Found';
				  }
				  ?>

			</div>

</div>
</div>

<?php get_footer(); ?>