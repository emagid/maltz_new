<?php get_header(); ?>


<div class="jumbotron" id="jumbotron_about" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/auction_fav.jpg)">
    <div class="banner_text" >
        <h1 class="uppercase">Favorites</h1>
    </div>
</div>


<div class="upcoming_auctions">
    <div class="featured_in_header">
            <img src="/wp-content/themes/Avada-Child-Theme/images/star.png">
			<h1><span>Following <?php echo do_shortcode('[user_favorite_count]'); ?> Auctions</span>  </h1>
         
        
		</div>
    <?php echo do_shortcode('[user_favorites]'); ?>  
</div>


<?php get_footer(); ?>