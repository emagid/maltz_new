<?php
/*
 * Template Name: Services Template
 */

get_header(); ?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

<div class="services_tab">
    <div>
        <a href="/services/auctions/">
            <img src="/wp-content/themes/Avada-Child-Theme/images/auctions_icon.png">
            <h6>Auctions</h6>
        </a>
    </div>
        <div>
            <a href="/services/appraisals/">
        <img src="/wp-content/themes/Avada-Child-Theme/images/appraisals_icon.png">
        <h6>Appraisals</h6>
            </a>
    </div>
        <div>
            <a href="/services/asset-record-storage/">
        <img src="/wp-content/themes/Avada-Child-Theme/images/storage_icon.png">
        <h6>Storage</h6>
            </a>
    </div>
        <div>
            <a href="/services/brokered-sales/">
        <img src="/wp-content/themes/Avada-Child-Theme/images/sales_icon.png">
        <h6>Brokered Sales</h6>
                </a>
    </div>
        <div>
            <a href="/services/equity-advance-program/">
        <img src="/wp-content/themes/Avada-Child-Theme/images/equity_icon.png">
        <h6>Equity Advance</h6>
            </a>
    </div>
        <div>
            <a href="/services/receivership/">
        <img src="/wp-content/themes/Avada-Child-Theme/images/receivership_icon.png">
        <h6>Receivership</h6>
            </a>
    </div>
            <div>
                <a href="/services/uccmezzsecurity-agreement/">
        <img src="/wp-content/themes/Avada-Child-Theme/images/auctions_icon.png">
        <h6>UCC / Mezzanine / Security Agreement</h6>
                </a>
    </div>
</div>

<div class="section_services">
    <div class="standard_contain">


		<div class="featured_in_header">
            <div class="blue_bubble">
			     <img src="<?php the_field('icon'); ?>">
                </div>
             <h1 ><span><?php echo get_the_title( $post_id ); ?></span></h1>
		</div>

		<div class="about_us_text">
            <p><?php the_field('content'); ?></p>

		</div>
        
	</div>
	
</div>

<div class="subscribers"  style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/blurred.jpg);">
	<h4>Weekly Maltz Auction Email Alerts</h4>

	<div class="subscribe_submit">
		<?php echo do_shortcode('[mailmunch-form id="651102"]'); ?>
		
	</div>

</div>

<?php get_footer(); ?>