<?php

get_header(); ?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>





<div class="upcoming_auctions">
		<div class="featured_in_header">
            <img src="/wp-content/themes/Avada-Child-Theme/images/auctions_icon.png">
			<h1><span><?php the_title(); ?></span></h1>
		</div>

		<div class="auction_holdings">

            
                        <?php
            $args = array(
      'post_type' => 'upcoming_auctions',
                'cat' => '17',
                'category__not_in' => 30
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
            <a id="url_og" href="<?php the_permalink(); ?>">
                <div class="auction_replaced">
                <div class="auction_holder trigger" style="background-image:url(<?php the_field('featured_image'); ?>);">
<!--                    <div class="overlay">-->

                                            <div class="fav_btn_holder">
                    <?php echo do_shortcode('[favorite_button]'); ?>
<!--                        </div>-->
                    </div>

                </div>
                                        <div class="auction_holder_contain" id="modified">
                            <div class="auction_detail">
                                <h4 style="color:#282a2b !important; margin-top:0;"><?php the_title( ); ?></h4>
                                <p style="color:#282a2b !important;"><?php the_field('city/state'); ?></p>
                                <p style="color:#282a2b !important;font-size:14px !important;"><?php the_field('auction_type'); ?></p>
                            </div>
                            <div class="auction_date">
                                <h6><?php the_field('month'); ?><br><span><?php the_field('date'); ?></span></h6>
                            </div>
                            
                        </div>
                </div>
            </a>
            
            
                        <?php
            }
                  }
            else  {
            echo 'There are currently no scheduled Jewelry & Collectibles auctions. Please visit again soon or join the Maltz Auctions mailing list for future notifications.';
            }
      ?> 

        </div>  
</div> 
            <?php
    get_sidebar('auctions');?>
    <!-- Trigger to open Modal -->
    


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<link rel="stylesheet" type="text/css" href="/wp-content/themes/Avada/js/libs/iziModal-master/css/iziModal.min.css"/>
    <script type="text/javascript" src="/wp-content/themes/Avada/js/libs/iziModal-master/js/iziModal.min.js"></script>



<!-- Modal structure -->
<!--
<div id="modal" class="subscribe_modal">
<div class="subscribers"  style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/jumbotron_img.png);">
	<h4>Sign up for Maltz Auction Alerts</h4>

	<div class="subscribe_submit">
		<//?php echo do_shortcode('[contact-form-7 id="710302" title="Auction Alerts"]'); ?>
		
	</div>
    
    <span id="url_new"><p>Or continue to auction</p></span>

</div>
-->

</div>

<!--
<script>
$(function() {
    $("a#url_og").click(function() {
        var thisurl = $(this).attr('href');
        
        $("#url_new").html('<a href="' + thisurl + '">' + 'Or continue to auction' + '</a>'); 
        });
    });
</script>

<script>
    
    
$("#modal").iziModal();
    
    $(document).on('click', '.trigger', function (event) {
    event.preventDefault();
    $('#modal').iziModal('open');
        
});
</script>
-->




<div class="subscribers"  style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/blurred_1.jpg);">
	<h4>Maltz Auction Alerts</h4>

	<div class="subscribe_submit">
		<?php echo do_shortcode('[mailmunch-form id="651102"]'); ?>
		
	</div>

</div>

<?php get_footer(); ?>