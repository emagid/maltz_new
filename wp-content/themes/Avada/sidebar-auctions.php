
<div class="upcoming_auctions">
		<div class="featured_in_header">
			<h1><span>All Upcoming Auctions</span></h1>
		</div>

		<div class="auction_holdings">

            
                        <?php
            $args = array(
      'post_type' => 'upcoming_auctions',
                'category__not_in' => 30,
                'posts_per_page' => 99,
                                'order' => 'ASC', 
                'meta_key' => '_expiration-date', 
                'orderby' => 'meta_value'
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
            <a id="url_og" href="<?php the_permalink(); ?>">
                <div class="auction_replaced">
                <div class="auction_holder trigger" style="background-image:url(<?php the_field('featured_image'); ?>);">
<!--                    <div class="overlay">-->

                                            <div class="fav_btn_holder">
                    <?php echo do_shortcode('[favorite_button]'); ?>
<!--                        </div>-->
                    </div>

                </div>
                                        <div class="auction_holder_contain" id="modified">
                            <div class="auction_detail">
                                <h4 style="color:#282a2b !important; margin-top:0;"><?php the_title( ); ?></h4>
                                <p style="color:#282a2b !important;"><?php the_field('city/state'); ?></p>
                                <p style="color:#282a2b !important;font-size:14px !important;min-height:20px;"><?php the_field('auction_type'); ?></p>
                            </div>
                            <div class="auction_date">
                                <h6><?php the_field('month'); ?><br><span><?php the_field('date'); ?></span></h6>
                            </div>
                            
                        </div>
                </div>
            </a>
            
            
                        <?php
            }
                  }
            else  {
            echo 'There are currently no scheduled auctions. Please visit again soon or join the Maltz Auctions mailing list for future notifications.';
            }
      ?> 

        </div>  
</div>
