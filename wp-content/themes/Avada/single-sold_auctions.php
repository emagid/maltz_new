<?php get_header(); ?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>
<style>
    .header_wrap {
        position: absolute;
    }
</style>
<div class="jumbotron" style="background-image:url(<?php the_field('featured_image'); ?>)">
    <div class="overlay" id="desktop_overlay">
    <div class="banner_text">
        <h1><?php the_title(); ?></h1>
        <h4>            <?php if (get_field('optional_title') != ''): ?>
            <?php the_field('optional_title'); ?>
         <?php endif; ?>
        </h4>

    </div>
        </div>
    
        <div class="action_buttons">
        <div class="fav_button">
               
                <?php echo do_shortcode('[favorite_button]'); ?>


        </div>
            <div class="share">
                <?php echo do_shortcode('[addtoany]'); ?>
                
                
            </div>
                        <div class="contact_trigger">
                <a>
                    <button class="btn_small">Inquire</button> 
                </a>
            </div>

    </div>
</div>


<div class="four_pillars" id="sticker">
        <?php

// vars	
$colors = get_field('image_size');


// check
if( $colors && in_array('original', $colors) ): ?>
    <style>
        .jumbotron {
            background-size:contain;
        }
    </style>
    
<?php endif; ?>  
    
        <?php

// vars	
$colors = get_field('background');


// check
if( $colors && in_array('yes', $colors) ): ?>
    <style>
.banner_text h4{
        background: rgba(50, 51, 67, 0.75);
}
    </style>
    
<?php endif; ?>  
    <div>
        <p id="auction_trigger"><img src="/wp-content/themes/Avada-Child-Theme/images/icon_info.png">Auction Info</p>
    </div>
        <div>
        <p id="photos_trigger"><img src="/wp-content/themes/Avada-Child-Theme/images/icon_info.png">Photos</p>
    </div>
        <div>
        <p id="property_trigger"><img src="/wp-content/themes/Avada-Child-Theme/images/icon_info.png">Auction Details</p>
    </div>
    <div>
        <p id="docs_trigger"><img src="/wp-content/themes/Avada-Child-Theme/images/icon_info.png">Documents</p>
    </div>
        <div>
        <p id="location_trigger"><img src="/wp-content/themes/Avada-Child-Theme/images/icon_info.png">Location</p>
    </div>
</div>

        <div class="about_us_snippets auction_snippets" id="auction_release">

            <div class="snippet_container">
                        <?php if (get_field('asset_address') != ''): ?>
                <div class="snippet">
                    <div class="featured_in_header">
                        <img class="og" src="/wp-content/themes/Avada-Child-Theme/images/icon_marker.png">
                    </div>
                    <h3>PROPERTY/ASSET LOCATION</h3>
                     <p><?php the_field('asset_address'); ?></p>
                </div>
            <?php endif; ?>
            
            <?php if (get_field('complete_auction_date_&_time') != ''): ?>
                <div class="snippet">
                    <div class="featured_in_header">
                         <img class="og" src="/wp-content/themes/Avada-Child-Theme/images/icon_date.png">
                    </div>
                    <h3>AUCTION DATE & TIME</h3>
                    <p><?php the_field('complete_auction_date_&_time'); ?></p>
                </div>
            <?php endif; ?>
            
            <?php if (get_field('deposit') != ''): ?>
                <div class="snippet">
                    <div class="featured_in_header">
                        <img class="og" src="/wp-content/themes/Avada-Child-Theme/images/icon_price.png">
                    </div>
                    <h3>Price Info</h3>
    <!--                <p><strong>Deposit: <?php the_field('deposit'); ?></strong></p>-->
                    <p>Scroll down for details</p>
                </div>
            <?php endif; ?>
            

            
            <?php if (get_field('auction_viewings') != ''): ?>
                 <div class="snippet">
                    <div class="featured_in_header">
                        <img class="og" src="/wp-content/themes/Avada-Child-Theme/images/icon_marker.png">
                    </div>
                    <h3>VIEWING</h3>
                     <p><?php the_field('auction_viewings'); ?></p>
<!--                     <p><//?php the_field('property_address'); ?></p>-->
                </div>
            <?php endif; ?>
            
            <?php if (get_field('auction_address') != ''): ?>
                <div class="snippet">
                    <div class="featured_in_header">
                        <img class="og" src="/wp-content/themes/Avada-Child-Theme/images/notification.png">
                    </div>
                    <h3>AUCTION LOCATION</h3>
                     <p><?php the_field('auction_address'); ?></p>
                </div>
            <?php endif; ?>
            
<!--
            <div class="snippet_bottom">
                <h6>Registration: <?php the_field('registration_time'); ?></h6>
                <p>Online and/or absentee bidding available with registration at least 5 business days prior to auction, please call for details. </p>
            </div>
-->
                <div>
        </div>

<div class="auction_photos" id="photos_release">
        <div class="standard_contain">
		<div class="featured_in_header">
			<h1><span>Photos</span></h1>
		</div>
        <?php the_field('gallery'); ?>
<!--        <//?php echo do_shortcode('	[URIS id=710594]'); ?>-->
    </div>
</div>

    <!-- Trigger to open Modal -->
    


	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		<link rel="stylesheet" type="text/css" href="/wp-content/themes/Avada/js/libs/iziModal-master/css/iziModal.min.css"/>
    <script type="text/javascript" src="/wp-content/themes/Avada/js/libs/iziModal-master/js/iziModal.min.js"></script>



<!-- Modal structure -->
<div id="modal">
<?php wp_login_form(); ?>
    <p id="trigger_reg">Or Register</p>
</div>

<!-- Modal structure -->
<div id="contact_modal">
    		<div class="featured_in_header">
                <button data-izimodal-close="">Close</button>
			<h1><span>Contact Us</span></h1>
		</div>
    <?php echo do_shortcode('[gravityform id=2 title=false description=false ajax=true tabindex=49]'); ?>
</div>



<script>
$("#modal").iziModal();
    
    $(document).on('click', '.trigger', function (event) {
    event.preventDefault();
    $('#modal').iziModal('open');
});
    
$("#contact_modal").iziModal();
    
    $(document).on('click', '.contact_trigger', function (event) {
    event.preventDefault();
    $('#contact_modal').iziModal('open');
});
</script>

<!--
    <?php

if (is_user_logged_in()) {
    
	echo ''; ?>

    <div class="auction_info" id="property_release">
    <?php
} else {
	echo ''; ?>
        <div class="div_trigger">
        <a><button class="btn_med trigger">Login to view Auction Details</button> </a>
            </div>
<div class="auction_info blur" id="property_release">
    <?php
}

?>
-->



    <div class="standard_contain" id="property_release">
<!--
        		<div class="featured_in_header">
			<h1><span>Auction Details</span></h1>
		</div>
-->
        		<div class="auction_info_header">
			<h1><?php the_title( ); ?></h1>
		</div>
            
        <div class="auction_info_subheader">
            <?php the_field('property_info'); ?>
            
            <p class="case_info" style="font-size:12pt;text-align:center;line-height:1.5em;"><?php the_field('case_info'); ?></p>
            <br>
            <h2 class="header" style="font-size:18pt;text-align:center;"><?php the_field('header'); ?></h2>
            <h4 class="subheader" style="font-size:14pt;text-align:center;"><?php the_field('subheader'); ?></h4>
            <h4 class="second_subheader" style="font-size:14pt;text-align:center;"><?php the_field('second_subheader'); ?></h4>
            <p style="font-size:14pt!important;font-weight:bold;text-align:center!important;">DETAILS</p>
            <div class="details" style="font-size:14pt!important;"><?php the_field('details'); ?></div>
            
           <style>
               .details p,
               .details ul,
               .details li {
                   font-size:14pt!important;line-height:1.5em!important;list-style-position:inside!important;
               }
            </style>
            
            <p><?php the_field('auction_info'); ?></p>
            
            
		</div>
        
        <div class="auction_info_content">


            
            <div class="about_us_section" id="location_release">
    <div class="standard_contain">

<?php if (get_field('form_1') != ''): ?>
		<div class="featured_in_header" id="docs_release">
			<h1><span>Documents/Additional Information</span></h1>
		</div>

		<div class="about_us_text">
            
        <div class="action_buttons auction_docs">
        <div class="about_us_snippets auction_snippets" id="auction_release">
            
   
                <div class="snippet">
                    <div class="featured_in_header">
                         <img class="og" src="/wp-content/themes/Avada-Child-Theme/images/document.png">
                    </div>
                    
                    <p><strong><?php the_field('form_1'); ?></strong></p>
                </div>
<?php endif; ?>
            
            <?php if (get_field('form_2') != ''): ?>
                <div class="snippet">
                    <div class="featured_in_header">
                         <img class="og" src="/wp-content/themes/Avada-Child-Theme/images/document.png">
                    </div>
                    <p><strong><?php the_field('form_2'); ?></strong></p>
                </div>
            <?php endif; ?>
            
            <?php if (get_field('form_3') != ''): ?>
                <div class="snippet">
                    <div class="featured_in_header">
                         <img class="og" src="/wp-content/themes/Avada-Child-Theme/images/document.png">
                    </div>
                    <p><strong><?php the_field('form_3'); ?></strong></p>
                </div>
            <?php endif; ?>
            
            <?php if (get_field('form_4') != ''): ?>
                <div class="snippet">
                    <div class="featured_in_header">
                         <img class="og" src="/wp-content/themes/Avada-Child-Theme/images/document.png">
                    </div>
                    <p><strong><?php the_field('form_4'); ?></strong></p>
                </div>
            <?php endif; ?>
            
            <?php if (get_field('form_5') != ''): ?>
                <div class="snippet">
                    <div class="featured_in_header">
                         <img class="og" src="/wp-content/themes/Avada-Child-Theme/images/document.png">
                    </div>
                    <p><strong><?php the_field('form_5'); ?></strong></p>
                </div>
            <?php endif; ?>
            
            <?php if (get_field('form_6') != ''): ?>
                <div class="snippet">
                    <div class="featured_in_header">
                         <img class="og" src="/wp-content/themes/Avada-Child-Theme/images/document.png">
                    </div>
                    <p><strong><?php the_field('form_6'); ?></strong></p>
                </div>
            <?php endif; ?>
            
                        <?php if (get_field('form_7') != ''): ?> 
                <div class="snippet">
                    <div class="featured_in_header">
                         <img class="og" src="/wp-content/themes/Avada-Child-Theme/images/document.png">
                    </div>
                    <p><strong><?php the_field('form_7'); ?></strong></p>
                </div>
            <?php endif; ?>
            
                        <?php if (get_field('form_8') != ''): ?>
                <div class="snippet">
                    <div class="featured_in_header">
                         <img class="og" src="/wp-content/themes/Avada-Child-Theme/images/document.png">
                    </div>
                    <p><strong><?php the_field('form_8'); ?></strong></p>
                </div>
            <?php endif; ?>
            
                        <?php if (get_field('form_9') != ''): ?>
                <div class="snippet">
                    <div class="featured_in_header">
                         <img class="og" src="/wp-content/themes/Avada-Child-Theme/images/document.png">
                    </div>
                    <p><strong><?php the_field('form_9'); ?></strong></p>
                </div>
            <?php endif; ?>
            
                        <?php if (get_field('form_10') != ''): ?>
                <div class="snippet">
                    <div class="featured_in_header">
                         <img class="og" src="/wp-content/themes/Avada-Child-Theme/images/document.png">
                    </div>
                    <p><strong><?php the_field('form_10'); ?></strong></p>
                </div>
            <?php endif; ?>

        </div>
        </div>
		</div>
        
        <?php if (get_field('links') != ''): ?>
        <div class="featured_in_header" id="docs_release" style="margin-top:40px;margin-bottom:80px;">
			<h1><span>Links</span></h1>
            <p><strong><?php the_field('links'); ?></strong></p>
		</div>
        <?php endif; ?>

        
	</div>
	
</div>

</div>
    </div>



<style>
    .blur {
        filter: progid:DXImageTransform.Microsoft.Blur(PixelRadius='4');
        -webkit-filter: url(#blur-filter);
        filter: url(#blur-filter);
        -webkit-filter: blur(4px);
        filter: blur(4px);
    }
    .blur-svg {
        display: none;
    }
</style>
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" class="blur-svg">
    <defs>
        <filter id="blur-filter">
            <feGaussianBlur stdDeviation="4"></feGaussianBlur>
        </filter>
    </defs>
</svg>

 
</div>





<div class="property_map">
    
    <div class="left_map">
        <div class="featured_in_header">
			<h1><span>Property/Asset Location</span></h1>
		</div>
        <?php the_field('property_map'); ?>
    </div>

    <div class="right_map">
        <div class="featured_in_header">
			<h1><span>Auction Location</span></h1>
		</div>
        <?php the_field('auction_map'); ?>
    </div>
</div>
            <div class="disclaimer"><?php the_field('disclaimer'); ?></div>
<div class="subscribers"  style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/blurred.jpg);">
	<h4>Weekly Maltz Auction Email Alerts</h4>

	<div class="subscribe_submit">
		<?php echo do_shortcode('[mailmunch-form id="651102"]'); ?>
		
	</div>

</div>


<script type="text/javascript">

$(document).ready(function(){
  jQuery('.slick_slider').slick({
  	        dots: false,
        infinite: true
  });
});
    
    $(".auction_holder").hover(function(){
    $(".overlay",this).slideToggle(500);
        
});
		
</script>

<script>
  $(document).ready(function(){
    $("#sticker").sticky({topSpacing:100});
  });
</script>

<script>
    $("#auction_trigger").click(function() {
    $('html, body').animate({
        scrollTop: $("#auction_release").offset().top-85
    }, 2000);
});
    $("#property_trigger").click(function() {
    $('html, body').animate({
        scrollTop: $("#property_release").offset().top-85
    }, 2000);
});
$("#photos_trigger").click(function() {
    $('html, body').animate({
        scrollTop: $("#photos_release").offset().top-85
    }, 2000);
});
    $("#location_trigger").click(function() {
    $('html, body').animate({
        scrollTop: $("#location_release").offset().top-85
    }, 2000);
});
$("#docs_trigger").click(function() {
    $('html, body').animate({
        scrollTop: $("#docs_release").offset().top-85
    }, 2000);
});

</script>

<script>
(function ($, root, undefined) {


   $(document).ready(function(){
	   $(window).bind('scroll', function() {
	   var navHeight = $( window ).height() - 85;
			 if ($(window).scrollTop() > navHeight) {
				 $('.four_pillars').addClass('fixed');
			 }
			 else {
				 $('.four_pillars').removeClass('fixed');
			 }
		});
	});

   })(jQuery, this);

</script>

<?php get_footer(); ?>