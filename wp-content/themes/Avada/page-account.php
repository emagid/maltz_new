<?php get_header(); ?>


<div class="jumbotron" id="jumbotron_about" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/news_banner.jpg)">
    <div class="banner_text" >
        <h1 class="uppercase">My Account</h1>
    </div>
</div>

<div class="member_contain ">
        <div class="member_block" id="favorites_trigger">
        <img src="/wp-content/themes/Avada-Child-Theme/images/receivership_icon.png">
        <h4>Favorites</h4>
    </div>
    
        <div class="member_block" id="email_trigger">
        <img src="/wp-content/themes/Avada-Child-Theme/images/receivership_icon.png">
        <h4>Email Preferences</h4>
    </div>
    <div class="member_block" id="password_trigger">
        <img src="/wp-content/themes/Avada-Child-Theme/images/receivership_icon.png">
        <h4>Change Password</h4>
    </div>
        

    

    
    <div class="member_block">
        <img src="/wp-content/themes/Avada-Child-Theme/images/receivership_icon.png">
        <a href="/logout"><h4>Logout</h4></a>
    </div> 
</div>



<div class="member_actions">
        <div class="member_act" id="favorites">
        <div class="featured_in_header">
            <h1><span>Favorites</span>  </h1>
        </div>
         <div class="member_act_content">
            <?php echo do_shortcode('[user_favorites]'); ?> 
		</div>
    </div>
    <div class="member_act" id="email_preferences">
        <div class="featured_in_header">
            <h1><span>Email Preferences</span>  </h1>
		</div>
        <div class="member_act_content">
            <?php echo do_shortcode('[contact-form-7 id="710661" title="Opt-In"]'); ?>
        </div>
    </div>    
    
        <div class="member_act" id="change_password">
        <div class="featured_in_header">
            <h1><span>Change Password</span>  </h1>
		</div>
        <div class="member_act_content">
            <?php echo do_shortcode('[ultimatemember_password]'); ?>
        </div>
    </div>
    
    
     

</div>

<div class="subscribers"  style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/blurred.jpg);">
	<h4>Weekly Maltz Auction Email Alerts</h4>

	<div class="subscribe_submit">
		<?php echo do_shortcode('[mailmunch-form id="651102"]'); ?>
		
	</div>

</div>


<script>
$("#favorites_trigger").click(function(){
        $('.member_block').removeClass('dark');
        $(this).addClass('dark');
        $(".member_act").hide();
        $(".member_act#favorites").show();
});
    
    $("#email_trigger").click(function(){
        $('.member_block').removeClass('dark');
        $(this).addClass('dark');
        $(".member_act").hide();
        $(".member_act#email_preferences").show();
});

    
    $("#password_trigger").click(function(){
        $('.member_block').removeClass('dark');
        $(this).addClass('dark');
        $(".member_act").hide();
        $(".member_act#change_password").show();
});


</script>

<?php get_footer(); ?>