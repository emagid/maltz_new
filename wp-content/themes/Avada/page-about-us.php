<?php get_header(); ?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

<div class="jumbotron" id="jumbotron_about" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/ballroom.jpg)">
    <div class="banner_text" >
        <h1 class="uppercase">About</h1>
    </div>
</div>


<div class="about_us_section">
    <div class="standard_contain">


		<div class="featured_in_header">
            <div class="blue_bubble">
			 <img src="/wp-content/themes/Avada-Child-Theme/images/justice-hammer.png">
                </div>
		</div>

		<div class="about_us_text">
            <p>Our 40-year-old firm is nationally recognized as a trusted name in excellence and achievement. Maltz Auctions is a premier, full-service auction company specializing in creating liquidity through the auction method of marketing. Now a $100+ million company with an impeccable reputation for professionalism, integrity and success, Maltz Auctions has conducted thousands of auctions and generated over $2 billion in sales on behalf of such clients as the U.S. Bankruptcy Courts and financial institutions throughout the country, the U.S. Department of Justice, the Securities and Exchange Commission, the Internal Revenue Service, N.Y. State Department of Taxation & Finance, Court Appointed Receivers, Estate Executors and Public Administrators, as well as law firms, private individuals and corporations-nationwide. Maltz Auctions is court approved to act in the capacity of auctioneers, real estate brokers and receivers.</p>
            
            <h3>Get In on the Action.</h3>
            
            <div class="holder_btn">
                <a href="/upcoming-auctions/">
                    <button class="btn_classic" id="extend_300">Upcoming Auctions</button>
                </a>
            </div>
		</div>
        
	</div>
	
</div>

<!--         <div class="about_us_snippets">
            <div class="snippet">
                <div class="featured_in_header">
                </div>
                <h3>BUYERS</h3>
                <p>Self Defined Price</p>
                <p>Simple Bidding Structure</p>
                <p>Conglomerated inventory</p>
                <p>Easy Transaction Process</p>
            </div>
            <div class="snippet">
                <h3>SELLERS</h3>
                <p>Fast Selling Opportunities</p>
                <p>International Connectivity</p>
                <p>Advanced Information Access</p>
                <p>Asset Appraisal</p>
            </div>
             <div class="snippet">
                <div class="featured_in_header">
                </div>
                <h3>FIDUCIARIES</h3>
                <p>Trustworthy Relationship</p>
                <p>Leading Marketing Strategies</p>
                <p>Client service availability</p>
                <p>Asset Assurance</p>
            </div>
        </div> -->



<div class="subscribers"  style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/blurred.jpg);">
	<h4>Weekly Maltz Auction Email Alerts</h4>

	<div class="subscribe_submit">
		<?php echo do_shortcode('[mailmunch-form id="651102"]'); ?>
		
	</div>

</div>

<?php get_footer(); ?>