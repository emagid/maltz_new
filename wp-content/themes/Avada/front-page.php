<?php get_header(); ?>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.min.js"></script>
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/jquery.slick/1.6.0/slick.css"/>

<div class="hero_slider">

    <?php
            $args = array(
      'post_type' => 'homepage_banners',
                'posts_per_page' => 5,
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
<div class="jumbotron" style="background-image:url(<?php the_field('image'); ?>)">
    <img class="sold" src="/wp-content/themes/Avada-Child-Theme/images/sold.png">
    <div class="banner_text">
        <h1><?php the_field('sold_price'); ?> - <?php the_field('location'); ?></h1>
        <a href="/upcoming-auctions">
            <h6>Upcoming Auctions</h6>
        </a>
    </div>
</div>
    
<!--
    <div class="homepage-hero-module">
    <div class="video-container">
        <video autoplay loop class="fillWidth">
            <source src="/wp-content/themes/Avada-Child-Theme/videos/maltz_video1.mp4" type="video/mp4" />Your browser does not support the video tag. I suggest you upgrade your browser.

    <div class="banner_text">
        <h1>Welcome to Maltz Auctions</h1>
        <h4>A premier, full-service auction company</h4>
        <a href="/upcoming-auctions">
            <h6>Upcoming Auctions</h6>
        </a>
    </div>
    </div>

</div>
-->

    <?php
            }
                  }
            else  {
            echo 'No Sold Auctions Found';
            }
      ?> 

    
    </div>

<div class="featured_in_section">
    <div class="standard_contain">


		<div class="featured_in_header">
			<h1><span>Showcased In</span></h1>
		</div>
        <a href="/in-the-news">
		<div class="featured_in_list ">
			<div>
				<img src="/wp-content/themes/Avada-Child-Theme/images/nyt.png">
			</div>
            <div>
				<img src="/wp-content/themes/Avada-Child-Theme/images/realtor_logo.png">
			</div>
            <div>
				<img src="/wp-content/themes/Avada-Child-Theme/images/newsday_logo.png">
			</div>
            <div>
				<img src="/wp-content/themes/Avada-Child-Theme/images/daily_logo.png">
			</div>
            <div>
				<img src="/wp-content/themes/Avada-Child-Theme/images/tandc_logo.png">
			</div>
            <div>
				<img src="/wp-content/themes/Avada-Child-Theme/images/wsj.png">
			</div>
                        <div>
				<img src="/wp-content/themes/Avada-Child-Theme/images/crains.png" style="max-width:120px;">
			</div>
		</div>
            </a>
	</div>
	
</div>


<div class="about_us_section">
    <div class="standard_contain">


		<div class="featured_in_header">
			<h1><span>About Us</span></h1>
		</div>

		<div class="about_us_text">
            <p>Maltz Auctions is a premier, full-service auction company specializing in creating liquidity through the auction method of marketing. Now a $100+ million company with an impeccable reputation for professionalism, integrity and success, Maltz Auctions has conducted thousands of auctions and generated over $2 billion in sales on behalf of such clients as the U.S. Bankruptcy Courts and financial institutions throughout the country, the U.S. Department of Justice, the Securities and Exchange Commission, the Internal Revenue Service, N.Y. State Department of Taxation & Finance, Court Appointed Receivers, Estate Executors and Public Administrators, as well as law firms, private individuals and corporations-nationwide. Maltz Auctions is court approved to act in the capacity of auctioneers, real estate brokers and receivers. The 40-year-old firm is nationally recognized as a trusted name in excellence and achievement.</p>
            
            <div class="holder_btn">
                <a href="/about-us">
                    <button class="btn_classic">Learn More</button>
                </a>
            </div>
		</div>
	</div>
	
</div>


<div class="upcoming_auctions">
		<div class="featured_in_header">
			<h1><span>Featured Auctions</span></h1>
		</div>

		<div class="auction_holdings" id="slicker">

     
                        <?php
            $args = array(
      'post_type' => 'upcoming_auctions',
                'category__not_in' => 30
      );
            $products = new WP_Query( $args );
                  if( $products->have_posts() ) {
            while( $products->have_posts() ) {
            $products->the_post();
      ?>
            <a id="url_og" href="<?php the_permalink(); ?>">
                <div class="auction_replaced" id="home_replaced">
                <div class="auction_holder trigger" style="background-image:url(<?php the_field('featured_image'); ?>);">
<!--                    <div class="overlay">-->

                                            <div class="fav_btn_holder">
                    <?php echo do_shortcode('[favorite_button]'); ?>
<!--                        </div>-->
                    </div>

                </div>
                                        <div class="auction_holder_contain" id="modified">
                            <div class="auction_detail">
                                <h4 style="color:#282a2b !important; margin-top:0;"><?php the_title( ); ?></h4>
                                <p style="color:#282a2b !important;"><?php the_field('city/state'); ?></p>
                            </div>
                            <div class="auction_date">
                                <h6><?php the_field('month'); ?><br><span><?php the_field('date'); ?></span></h6>
                            </div>
                            
                        </div>
                </div>
            </a>
            
                        <?php
            }
                  }
            else  {
            echo 'No Auctions Found';
            }
      ?> 


        </div>  
</div> 

<div class="upcoming_auctions" id="upcoming_auctions_alt">
		<div class="featured_in_header">
			<h1><span><a href="/upcoming-auctions/">Upcoming Auctions</a></span></h1>
		</div>

		<div class="auction_holdings">
		
            <a href="/upcoming-auctions/residential-properties">
                <div class="auction_holder" id="row_3" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_res.jpg);">
                    <div class="overlay">
                        <div class="auction_holder_contain">
                            <div class="auction_detail">
                                <h5>Residential Properties</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            
            <a href="/upcoming-auctions/commercial-properties">
            <div class="auction_holder" id="row_3" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_comm.jpg);">
                <div class="overlay">
                    <div class="auction_holder_contain">
                        <div class="auction_detail">
                            <h5>Commercial Properties</h5>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            
            <a href="/upcoming-auctions/distinctive-properties">
            <div class="auction_holder" id="row_3" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_dist.jpg);">
                <div class="overlay">
                    <div class="auction_holder_contain">
                        <div class="auction_detail">
                            <h5>Distinctive Properties</h5>
                        </div>
                    </div>
                </div>
            </div>
            </a>
        </div>  
    
    		<div class="featured_in_header">
			<h3>We offer more than real estate. Additional auction divisions:</h3>
		</div>

		<div class="auction_holdings">
		  
            <a href="/upcoming-auctions/autos-boats/">
            <div class="auction_holder" id="row_3" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_auto.jpg);">
                <div class="overlay">
                    <div class="auction_holder_contain">
                        <div class="auction_detail">
                            <h5>Autos & Boats</h5>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            
            <a href="/upcoming-auctions/jewelry-collectibles/">
            <div class="auction_holder" id="row_3" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_jewel.jpg);">
                <div class="overlay">
                    <div class="auction_holder_contain">
                        <div class="auction_detail">
                            <h5>Jewelry & Collectibles</h5>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            
            <a href="/upcoming-auctions/taxi-medallions/">
            <div class="auction_holder" id="row_3" style="background-image:url(https://taximedallionauction.com/wp-content/themes/emagid/assets/img/taxi-cab.jpg);">
                <div class="overlay">
                    <div class="auction_holder_contain">
                        <div class="auction_detail">
                            <h5>Taxi Medallions</h5>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            
            <a href="/upcoming-auctions/heavy-equipment/">
            <div class="auction_holder" id="row_3" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_heavy.jpg);">
                <div class="overlay">
                    <div class="auction_holder_contain">
                        <div class="auction_detail">
                            <h5>Heavy Equipment</h5>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            
            <a href="/upcoming-auctions/luxury-assets/">
            <div class="auction_holder" id="row_3" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_boats.jpg);">
                <div class="overlay">
                    <div class="auction_holder_contain">
                        <div class="auction_detail">
                            <h5>Luxury Assets</h5>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            
            <a href="/upcoming-auctions/going-concerns-leaseholds/">
            <div class="auction_holder" id="row_3" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_lease.jpg);">
                <div class="overlay">
                    <div class="auction_holder_contain">
                        <div class="auction_detail">
                            <h5>Going Concerns & Leaseholds</h5>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            
            <a href="/upcoming-auctions/business_assets_inventory/">
            <div class="auction_holder" id="row_3" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_business.jpg);">
                <div class="overlay">
                    <div class="auction_holder_contain">
                        <div class="auction_detail">
                            <h5>Business Assets & Inventory</h5>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            
            <a href="/upcoming-auctions/unique-assets/">
            <div class="auction_holder" id="row_3" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_unique.jpg);">
                <div class="overlay">
                    <div class="auction_holder_contain">
                        <div class="auction_detail">
                            <h5>Unique Assets</h5>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            
            <a href="/services/equity-advance-program/">
            <div class="auction_holder" id="row_3" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/prop_equity.jpg);">
                <div class="overlay">
                    <div class="auction_holder_contain">
                        <div class="auction_detail">
                            <h5>Equity Advance</h5>
                        </div>
                    </div>
                </div>
            </div>
            </a>
            
        </div>  
</div> 
<div class="test_section">

			<div class="featured_in_header">
			<h1><span>Testimonials</span></h1>
		</div>
<div class="wrapper">
<h4>Testimonials from a few of our past sellers, buyers, brokers, and fiduciaries.</h4>
			
			<div class="testimonials_slider" id="sellers">

					<?php
			  $args = array(
			    'post_type' => 'testimonials',
                  'cat' => '26'
                  
			    );
			  $products = new WP_Query( $args );
			  if( $products->have_posts() ) {
			    while( $products->have_posts() ) {
			      $products->the_post();
			?>

				<div class="quote_box">
                    <h6>"</h6>
					<blockquote><?php the_field('quote'); ?></blockquote>
					<p>- <?php the_field('author'); ?><p>
				</div>

				<?php
			    		}
			  		}
				  else {
				    echo 'No Testimonials Found';
				  }
				  ?>

			</div>
    
            <div class="testimonials_slider" id="buyers" style="display:none;">

					<?php
			  $args = array(
			    'post_type' => 'testimonials',
                  'cat' => '25'
			    );
			  $products = new WP_Query( $args );
			  if( $products->have_posts() ) {
			    while( $products->have_posts() ) {
			      $products->the_post();
			?>

				<div class="quote_box">
                    <h6>"</h6>
					<blockquote><?php the_field('quote'); ?></blockquote>
					<p>- <?php the_field('author'); ?><p>
				</div>

				<?php
			    		}
			  		}
				  else {
				    echo 'No Testimonials Found';
				  }
				  ?>

			</div>
    
            <div class="testimonials_slider" id="fiduciaries" style="display:none;">

					<?php
			  $args = array(
			    'post_type' => 'testimonials',
                  'cat' => '27'
			    );
			  $products = new WP_Query( $args );
			  if( $products->have_posts() ) {
			    while( $products->have_posts() ) {
			      $products->the_post();
			?>

				<div class="quote_box">
                    <h6>"</h6>
					<blockquote><?php the_field('quote'); ?></blockquote>
					<p>- <?php the_field('author'); ?><p>
				</div>

				<?php
			    		}
			  		}
				  else {
				    echo 'No Testimonials Found';
				  }
				  ?>

			</div>
    
            <div class="testimonials_slider" id="brokers" style="display:none;">

					<?php
			  $args = array(
			    'post_type' => 'testimonials',
                  'cat' => '32'
			    );
			  $products = new WP_Query( $args );
			  if( $products->have_posts() ) {
			    while( $products->have_posts() ) {
			      $products->the_post();
			?>

				<div class="quote_box">
                    <h6>"</h6>
					<blockquote><?php the_field('quote'); ?></blockquote>
					<p>- <?php the_field('author'); ?><p>
				</div>

				<?php
			    		}
			  		}
				  else {
				    echo 'No Testimonials Found';
				  }
				  ?>

			</div>
</div>
        <div class="test_select" >
        <div class="test_press highlight" id="sellers_trig">
            <h3>Sellers</h3>
        </div>
        <div class="test_press" id="buyers_trig">
            <h3>Buyers</h3>
        </div>
        <div class="test_press" id="fiduciaries_trig">
            <h3>Fiduciaries</h3>
        </div>
        <div class="test_press" id="brokers_trig">
            <h3>Brokers</h3>
        </div>
    </div>
</div>

<script>
$('.test_press#sellers_trig').click(function() {
    $('#buyers').hide(); 
    $('#fiduciaries').hide();
    $('#brokers').hide(); 
    $('#sellers').show(); 
      $('#sellers').slick("refresh");
});
$('.test_press#buyers_trig').click(function() { 
    $('#fiduciaries').hide();
    $('#brokers').hide(); 
    $('#sellers').hide(); 
    $('#buyers').show(); 
      $('#buyers').slick("refresh");
});
$('.test_press#fiduciaries_trig').click(function() {
        $('#buyers').hide(); 
    $('#brokers').hide(); 
    $('#sellers').hide(); 
    $('#fiduciaries').show(); 
      $('#fiduciaries').slick("refresh");
});
    
$('.test_press#brokers_trig').click(function() {
    $('#buyers').hide(); 
    $('#fiduciaries').hide();
    $('#sellers').hide(); 
    $('#brokers').show(); 
     $('#brokers').slick("refresh");
});
</script>


<div class="subscribers"  style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/blurred.jpg);">
	<h4>Weekly Maltz Auction Email Alerts</h4>

	<div class="subscribe_submit">
		<?php echo do_shortcode('[mailmunch-form id="651102"]'); ?>
		
	</div>

</div>

<script>
$( ".test_press" ).click(function() {
    $( ".test_press" ).removeClass( "highlight" );
  $( this ).addClass( "highlight" );
});
</script>
<script type="text/javascript">

$(document).ready(function(){
  jQuery('.hero_slider').slick({
  	        dots: false,
        infinite: true,
      arrows: true,
      autoplay: true,
  autoplaySpeed: 4000
  });
});
    
$(document).ready(function(){
  jQuery('.auction_holdings#slicker').slick({
  	        dots: false,
        infinite: true,
      arrows: false,
      autoplay: true,
      slidesToShow: 3,
  slidesToScroll: 1,
  autoplaySpeed: 4000,
        responsive: [
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
          adaptiveHeight: true
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
  });
});

    
    
    
//    $(".auction_holder").hover(function(){
//    $(".overlay",this).slideToggle(500);
//        $('.homepage-hero-module').find('video').get(0).pause();
//        
//});
		
</script>


<script>
//jQuery is required to run this code
$( document ).ready(function() {

    scaleVideoContainer();

    initBannerVideoSize('.video-container .poster img');
    initBannerVideoSize('.video-container .filter');
    initBannerVideoSize('.video-container video');

    $(window).on('resize', function() {
        scaleVideoContainer();
        scaleBannerVideoSize('.video-container .poster img');
        scaleBannerVideoSize('.video-container .filter');
        scaleBannerVideoSize('.video-container video');
    });

});

function scaleVideoContainer() {

    var height = $(window).height() + 5;
    var unitHeight = parseInt(height) + 'px';
    $('.homepage-hero-module').css('height',unitHeight);

}

function initBannerVideoSize(element){

    $(element).each(function(){
        $(this).data('height', $(this).height());
        $(this).data('width', $(this).width());
    });

    scaleBannerVideoSize(element);

}

function scaleBannerVideoSize(element){

    var windowWidth = $(window).width(),
    windowHeight = $(window).height() + 5,
    videoWidth,
    videoHeight;

    // console.log(windowHeight);

    $(element).each(function(){
        var videoAspectRatio = $(this).data('height')/$(this).data('width');

        $(this).width(windowWidth);

        if(windowWidth < 1000){
            videoHeight = windowHeight;
            videoWidth = videoHeight / videoAspectRatio;
            $(this).css({'margin-top' : 0, 'margin-left' : -(videoWidth - windowWidth) / 2 + 'px'});

            $(this).width(videoWidth).height(videoHeight);
        }

        $('.homepage-hero-module .video-container video').addClass('fadeIn animated');

    });
}
</script>

<?php get_footer(); ?>