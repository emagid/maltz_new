<?php

// Exit if accessed directly
if ( !defined('ABSPATH')) exit;

/**
 * This child theme functions.php file
 * does not override the parent theme functions.php file
 *  
 * Instead it adds anything below to the original functions.php contents.
 */


/* Wordpress Custom Login Page */
function custom_login_css() {
  echo '<link rel="stylesheet" type="text/css" href="'.get_stylesheet_directory_uri().'/login/login-styles.css" />';
}

add_action('login_head', 'custom_login_css'); 

/* Add Google Fonts for Custom Login Page */
function custom_fonts() {
  echo '<link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700" rel="stylesheet" type="text/css">';
}

add_action('login_head', 'custom_fonts');

/* Change Custom Login Page Logo Url */
add_filter( 'login_headerurl', 'custom_login_header_url' );

function custom_login_header_url($url) {
  return 'http://acmeauctionco.com/';
}