<?php get_header(); ?>

	<div id="content" class="full-width auction_det_contain">
		<?php while(have_posts()): the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php global $data; if($data['featured_images'] && has_post_thumbnail()): ?>
			
			<?php endif; ?>
			<div class="post-content">
				<?php the_content(); ?>
			</div>
		</div>
		<?php endwhile; ?>
	</div>

	<div class="bottom_portion">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3020.5745091826525!2d-73.18112698439545!3d40.79336747932313!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89e836d5ab13371b%3A0x4a2f9ac139663b08!2s39+Windsor+Pl%2C+Central+Islip%2C+NY+11722!5e0!3m2!1sen!2sus!4v1496423621476" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>

	<div class="upcoming_auctions" style="background-image:url(/wp-content/themes/Avada-Child-Theme/images/maltz_bg.png);">

<div class="subscribe_now">
<h3>Our latest auctions, straight to your phone.</h3>
<h2>Receive Auction Updates via Text</h2>
<p>I'm interested in...</p>

<form action="">
	<div class="input">
  		<input type="radio" name="gender" value="Real Estate"><p>Real Estate</p></input><br>
  	</div>
  	<div class="input">
  		<input type="radio" name="gender" value="Jewelry"> <p>Jewelry</p></input><br>
  	</div>
  	<div class="input">
	  	<input type="radio" name="gender" value="Auto & Boats"><p> Auto & Boats</p></input><br>
	 </div>

<div class="subscribe_submit">
<input type="text" placeholder="Phone #"></input><input type="submit"></input>
</div>

</form>

</div>
<?php get_footer(); ?>


<style type="text/css">
body.single-auction #main {
	padding: 0 !important;
}
.auction_det_contain .auctionTitle {
	background: url('/wp-content/themes/Avada-Child-Theme/images/nice_view.jpg');
	height: 550px;
	background-size: cover;
	background-repeat: no-repeat;
}
.auction_det_contain #acDetailWrap h1 {
	margin-top: 0 !important;
}
.auction_det_contain .auctionTitle > h1:first-child {
	padding-top: 150px;
	padding-bottom: 50px;
	font-family: 'PF Encore Sans Pro Bold' !important;
    -webkit-font-smoothing: antialiased;
    font-size: 70px !important;
    letter-spacing: 1px;
    text-transform: uppercase; !important;
    color: #fff !important;
        text-align: center;
}
.auction_det_contain .auctionTitle > h1:last-child {
	font-family: 'PF Encore Sans Pro Book' !important;
    -webkit-font-smoothing: antialiased;
    font-size: 35px !important;
    letter-spacing: 1px;
    text-transform: uppercase; !important;
    color: #fff !important;
        text-align: center;
}
.auction_det_contain #summaryWrap {
	/*float: none !important;*/
	margin: 0 auto;
	padding: 14px;
	/*width: 100%;*/
	background-color: #353644 !important;
}

#summaryWrap #summaryPic {
	margin: 0 auto !important;
	float: none !important;
}

#summaryWrap #summarySubtitle h2 {
	font-family: 'PF Encore Sans Pro Book' !important;
    -webkit-font-smoothing: antialiased;
    padding-top: 20px;
    font-size: 35px !important;
    letter-spacing: 1px;
    text-transform: uppercase; !important;
    color: #fff !important;
        text-align: center;
}

#summaryWrap #summaryInfo p {
		font-family: 'PF Encore Sans Pro Book' !important;
    -webkit-font-smoothing: antialiased;
	font-size: 16px !important;
	color: #fff !important;
	text-align: center;
	padding-top: 8px;
	margin-left: 0 !important;
}

.viewportHD {
	border-bottom-color: rgba(111, 111, 122, 0.5) !important;
	margin-bottom: 50px;

}
.viewportHD h2 {
	font-family: 'PF Encore Sans Pro Book' !important;
    -webkit-font-smoothing: antialiased;
    font-size: 38px !important;
    letter-spacing: 1px;
    text-transform: uppercase;
    color: #282a2b !important;
    text-align: center !important;
    margin-bottom: 20px !important;
}
.slideWrap, .sliderwrapper {
	width: 80% !important;
	margin: 0 auto !important;
}
.sliderwrapper .contentdiv {
	left: 0;
	right: 0;
	margin:  auto;
}
p.clickImage {
	font-family: 'PF Encore Sans Pro Book' !important;
    font-size: 13px !important;
    letter-spacing: 0.5px;
    margin-bottom: 0;
}

#acDetailFooter {
	display: none !important;
}
.bottom_portion {
	padding-top: 50px;
	clear: both;

}
</style>